import { _decorator, CCInteger, Component, Game, instantiate, Label, log, Node, Prefab, Vec3 } from 'cc';
import { PlayerController } from './PlayerController';
const { ccclass, property } = _decorator;

enum BlockType {
    BT_NONE,
    BT_STONE,
}

enum GameState {
    GS_INIT,
    GS_PLAYING,
    GS_END,
    GS_END_REVIVE,
};

@ccclass('GameManager')
export class GameManager extends Component {

    @property({ type: Prefab })
    public cubePrfb: Prefab | null = null;

    @property({ type: CCInteger })
    roadLength = 50;

    @property({ type: PlayerController })
    public playerCtrl: PlayerController | null = null;

    @property({ type: Node })
    public startMenu: Node | null = null;

    @property({ type: Node })
    public resultMenu: Node | null = null;

    @property({ type: Node })
    public nextButton: Node | null = null;

    @property({ type: Node })
    public reviveButton: Node | null = null;

    @property({ type: Label })
    public stepsLabel: Label | null = null;

    @property({ type: Label })
    public scoure: Label | null = null;

    private _road: BlockType[] = []

    start() {
        this.curState = GameState.GS_INIT;
        this.playerCtrl.node.on('JumpEnd', this.onPlayerJumpEnd, this)
    }

    set curState(value: GameState) {
        switch (value) {

            case GameState.GS_PLAYING:
                this.startMenu.active = false;
                setTimeout(() => {
                    if (this.playerCtrl) {
                        this.playerCtrl.setInputActive(true);
                    }
                }, 0.1);
                break;
            case GameState.GS_END:
                this.nextLevel();
                break;
            case GameState.GS_END_REVIVE:
                this.revive();
                break;
            default:
                this.init();
        }
    }

    init() {
        this.generateRoad();
        this.startMenu.active = true;
        this.playerCtrl.setInputActive(false);
        this.playerCtrl.node.setPosition(Vec3.ZERO);
        this.playerCtrl.reset();
        this.resultMenu.active = false;
    }

    revive() {
        this.startMenu.active = false;
        this.resultMenu.active = true;
        this.nextButton.active = false;
        this.reviveButton.active= true;
        this.playerCtrl.setInputActive(false);
        this.scoure.string = this.playerCtrl.curMoveIndex.toString();
    }

    nextLevel(){
        this.generateRoad();
        this.startMenu.active = false;
        this.resultMenu.active = true;
        this.nextButton.active = true;
        this.reviveButton.active = false;
        this.playerCtrl.setInputActive(false);
        this.playerCtrl.node.setPosition(Vec3.ZERO)
        this.playerCtrl.reset();
        this.scoure.string = this.roadLength.toString();
    }

    

    generateRoad() {
        this.node.removeAllChildren()

        this._road = [];

        this._road.push(BlockType.BT_STONE);

        for (let i = 1; i < this.roadLength; i++) {
            if (this._road[i - 1] === BlockType.BT_NONE) {
                this._road.push(BlockType.BT_STONE);
            } else {
                this._road.push(Math.floor(Math.random() * 2));
            }
        }

        for (let j = 0; j < this.roadLength; j++) {
            const child = this.spwanBlockByType(this._road[j]);
            if (child) {
                this.node.addChild(child);
                child.setPosition(j, 0.5, 0);
            }
        }
    }

    spwanBlockByType(type: BlockType) {
        if (!this.cubePrfb) {
            return null;
        }

        let block: Node | null = null;
        switch (type) {
            case BlockType.BT_STONE:
                block = instantiate(this.cubePrfb);
                break;
        }

        return block;
    }

    onStartButtonClicked() {
        this.curState = GameState.GS_PLAYING;
    }

    checkResult(moveIndex: number) {
        if (moveIndex < this.roadLength -1) {
            if (this._road[moveIndex] === BlockType.BT_NONE) {
                console.log(moveIndex);
                
                this.curState = GameState.GS_END_REVIVE;
            }
            else {
                this.curState = GameState.GS_END;
            }
        }
    }

    onPlayerJumpEnd(moveIndex: number) {
        this.checkResult(moveIndex);
    }

    onReviveButtonClicked() {
        this.onNextButtonClicked();
        this.playerCtrl.revive();
        this.playerCtrl.node.setPosition(this.playerCtrl.curMoveIndex,0,0);
    }

    onNextButtonClicked() {
        this.resultMenu.active = false;
        setTimeout(() =>{
            this.playerCtrl.setInputActive(true);
        }, 0.1)
    }
}


